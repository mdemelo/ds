﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.DirectoryServices.AccountManagement;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;



namespace MouDS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void buttonSalvaArquivos_Click(object sender, EventArgs e)
        {
            try
            {
                string PathConfig = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + @"\Fujitsu general\DesignSimulator";
                string PathInstall = @"D:\Program Files\DesignSimulator_Fujitsu";

                if (Directory.Exists(PathConfig))
                {

                    //string[] str = {Properties.Resources.UnitPrice_F};

                    File.WriteAllText(PathConfig + "\\BasicInfo.config", Properties.Resources.BasicInfo);
                    File.WriteAllText(PathConfig + "\\DispInfo.config", Properties.Resources.DispInfo);
                    File.WriteAllText(PathConfig + "\\DXKitMaster.config", Properties.Resources.DXKitMaster);
                    File.WriteAllText(PathConfig + "\\FGL_DispItem.config", Properties.Resources.FGL_DispItem);
                    File.WriteAllText(PathConfig + "\\FGL_DSM01.config", Properties.Resources.FGL_DSM01);
                    File.WriteAllText(PathConfig + "\\FGL_FloorItemVisible.config", Properties.Resources.FGL_FloorItemVisible);
                    File.WriteAllText(PathConfig + "\\FGL_IndrSrtItem.config", Properties.Resources.FGL_IndrSrtItem);
                    File.WriteAllText(PathConfig + "\\FGL_LumpItem.config", Properties.Resources.FGL_LumpItem);
                    File.WriteAllText(PathConfig + "\\FGL_StartMode.config", Properties.Resources.FGL_StartMode);
                    File.WriteAllText(PathConfig + "\\FGL_UOption.fgf", Properties.Resources.FGL_UOption);
                    
                    File.WriteAllText(PathConfig + "\\FGL_UserdefaultPower.config", Properties.Resources.FGL_UserdefaultPower);
                    File.WriteAllText(PathConfig + "\\FGL_UserdefaultTotalPower.config", Properties.Resources.FGL_UserdefaultTotalPower);
                    File.WriteAllText(PathConfig + "\\PipeSellAmount.config", Properties.Resources.PipeSellAmount);
                    File.WriteAllText(PathConfig + "\\RefrgSellAmount.config", Properties.Resources.RefrgSellAmount);

                    File.WriteAllText(PathConfig + "\\UnitPrice_F.config", Properties.Resources.UnitPrice_F);
                    File.WriteAllText(PathConfig + "\\UnitPrice_G.config", Properties.Resources.UnitPrice_G);



                    if (Directory.Exists(PathInstall))
                    {
                        File.WriteAllText(PathInstall + "\\HeaderFujitsu.rtf", Properties.Resources.HeaderFujitsu);
                        File.WriteAllText(PathInstall + "\\HeaderGeneral.rtf", Properties.Resources.HeaderGeneral);
                        File.WriteAllText(PathInstall + "\\Language\\French.xml", Properties.Resources.French);


                        MessageBox.Show("Mise à jour terminé!");

                        Close();
                    }
                    else
                    {
                        MessageBox.Show("Veuillez indiquer le dossier d'installation de Design Simulator.\n(Pour connaitre ce chemin, faire clic droit sur le raccourci et 'Ouvrir l'emplacement du fichier')", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        try
                        {

                            FolderBrowserDialog dialog = new FolderBrowserDialog();

                            dialog.RootFolder = Environment.SpecialFolder.Desktop;

                            dialog.ShowNewFolderButton = false;

                            if (dialog.ShowDialog() == DialogResult.OK)
                            {
                                PathInstall = dialog.SelectedPath;
                                File.WriteAllText(PathInstall + "\\HeaderFujitsu.rtf", Properties.Resources.HeaderFujitsu);
                                File.WriteAllText(PathInstall + "\\HeaderGeneral.rtf", Properties.Resources.HeaderGeneral);
                                File.WriteAllText(PathInstall + "\\Language\\French.xml", Properties.Resources.French);
                                MessageBox.Show("Mise à jour terminé!");

                                Close();
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }
                else
                {
                    MessageBox.Show("Design Simulator n'a pas été detécté dans cette machine.\nVeuillez  instaler le logiciel, puis relancer cette mise à jour.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
    }
}
