﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MouDS.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MouDS.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;BasicInfo&gt;
        ///    &lt;xIndex&gt;1&lt;/xIndex&gt;
        ///    &lt;Master_Info&gt;2&lt;/Master_Info&gt;
        ///    &lt;Use_OtherCmpny_Data&gt;0&lt;/Use_OtherCmpny_Data&gt;
        ///    &lt;Use_OtherCmpny_Sim&gt;0&lt;/Use_OtherCmpny_Sim&gt;
        ///    &lt;Tax_Div&gt;0&lt;/Tax_Div&gt;
        ///    &lt;Delivery_Pos /&gt;
        ///    &lt;Delivery_date /&gt;
        ///    &lt;Payment_Condition /&gt;
        ///    &lt;Expiry_Date /&gt;
        ///    &lt;Remarks /&gt;
        ///    &lt;Tax_rate&gt;0&lt;/Tax_rate&gt;
        ///    &lt;Rounding&gt;0&lt;/Rounding&gt;
        ///    &lt;LCID&gt;1031&lt;/LCID&gt;
        ///    &lt;Decimal_PointNumber&gt;0&lt;/Decimal_PointNumber&gt;
        ///    &lt;Inputformat&gt; [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string BasicInfo {
            get {
                return ResourceManager.GetString("BasicInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;DispInfo&gt;
        ///    &lt;ItemNo&gt;1&lt;/ItemNo&gt;
        ///    &lt;Mainte_disp&gt;1&lt;/Mainte_disp&gt;
        ///    &lt;Estimete_disp&gt;1&lt;/Estimete_disp&gt;
        ///    &lt;Estimete_Out&gt;1&lt;/Estimete_Out&gt;
        ///    &lt;Remarks /&gt;
        ///    &lt;Lock&gt;1&lt;/Lock&gt;
        ///  &lt;/DispInfo&gt;
        ///  &lt;DispInfo&gt;
        ///    &lt;ItemNo&gt;2&lt;/ItemNo&gt;
        ///    &lt;Mainte_disp&gt;1&lt;/Mainte_disp&gt;
        ///    &lt;Estimete_disp&gt;1&lt;/Estimete_disp&gt;
        ///    &lt;Estimete_Out&gt;1&lt;/Estimete_Out&gt;
        ///    &lt;Remarks /&gt;
        ///    &lt;Lock&gt;1&lt;/Lock&gt;
        ///  &lt;/DispInfo&gt;
        ///  &lt;DispInfo&gt;
        ///    &lt;ItemNo&gt;3&lt;/ItemNo&gt;
        ///    &lt;Mainte_disp&gt;1&lt; [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string DispInfo {
            get {
                return ResourceManager.GetString("DispInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement /&gt;.
        /// </summary>
        internal static string DXKitMaster {
            get {
                return ResourceManager.GetString("DXKitMaster", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;DispItem&gt;
        ///    &lt;xIndex&gt;1&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;0&lt;/DispMode&gt;
        ///  &lt;/DispItem&gt;
        ///  &lt;DispItem&gt;
        ///    &lt;xIndex&gt;2&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;1&lt;/DispMode&gt;
        ///  &lt;/DispItem&gt;
        ///  &lt;DispItem&gt;
        ///    &lt;xIndex&gt;3&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;0&lt;/DispMode&gt;
        ///  &lt;/DispItem&gt;
        ///  &lt;DispItem&gt;
        ///    &lt;xIndex&gt;4&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;0&lt;/DispMode&gt;
        ///  &lt;/DispItem&gt;
        ///  &lt;DispItem&gt;
        ///    &lt;xIndex&gt;5&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;1&lt;/DispMode&gt;
        ///  &lt;/DispItem&gt;
        ///  &lt;DispItem&gt;
        ///    &lt;xIndex&gt;6&lt;/xIndex&gt;
        ///    &lt;DispM [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_DispItem {
            get {
                return ResourceManager.GetString("FGL_DispItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;Preference&gt;
        ///    &lt;Language&gt;3&lt;/Language&gt;
        ///    &lt;Destination&gt;0&lt;/Destination&gt;
        ///    &lt;Brand&gt;0&lt;/Brand&gt;
        ///    &lt;Temperature&gt;0&lt;/Temperature&gt;
        ///    &lt;Airflow&gt;0&lt;/Airflow&gt;
        ///    &lt;Length&gt;0&lt;/Length&gt;
        ///    &lt;Capacity&gt;0&lt;/Capacity&gt;
        ///    &lt;Dimension&gt;0&lt;/Dimension&gt;
        ///    &lt;IndoorCondition&gt;0&lt;/IndoorCondition&gt;
        ///    &lt;Piping&gt;1&lt;/Piping&gt;
        ///    &lt;Weight&gt;0&lt;/Weight&gt;
        ///    &lt;FlowPress&gt;1&lt;/FlowPress&gt;
        ///    &lt;PassWord /&gt;
        ///    &lt;IndoorName&gt;Indr&lt;/IndoorName&gt;
        ///    &lt;OutdoorName&gt;Otdr&lt;/OutdoorName&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_DSM01 {
            get {
                return ResourceManager.GetString("FGL_DSM01", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;Preference&gt;
        ///    &lt;Language&gt;3&lt;/Language&gt;
        ///    &lt;Destination&gt;0&lt;/Destination&gt;
        ///    &lt;Brand&gt;0&lt;/Brand&gt;
        ///    &lt;Temperature&gt;0&lt;/Temperature&gt;
        ///    &lt;Airflow&gt;0&lt;/Airflow&gt;
        ///    &lt;Length&gt;0&lt;/Length&gt;
        ///    &lt;Capacity&gt;0&lt;/Capacity&gt;
        ///    &lt;Dimension&gt;0&lt;/Dimension&gt;
        ///    &lt;IndoorCondition&gt;0&lt;/IndoorCondition&gt;
        ///    &lt;Piping&gt;1&lt;/Piping&gt;
        ///    &lt;Weight&gt;0&lt;/Weight&gt;
        ///    &lt;FlowPress&gt;1&lt;/FlowPress&gt;
        ///    &lt;PassWord /&gt;
        ///    &lt;IndoorName&gt;Indr&lt;/IndoorName&gt;
        ///    &lt;OutdoorName&gt;Otdr&lt;/OutdoorName&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_DSM01_F {
            get {
                return ResourceManager.GetString("FGL_DSM01_F", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;Preference&gt;
        ///    &lt;Language&gt;3&lt;/Language&gt;
        ///    &lt;Destination&gt;0&lt;/Destination&gt;
        ///    &lt;Brand&gt;0&lt;/Brand&gt;
        ///    &lt;Temperature&gt;0&lt;/Temperature&gt;
        ///    &lt;Airflow&gt;0&lt;/Airflow&gt;
        ///    &lt;Length&gt;0&lt;/Length&gt;
        ///    &lt;Capacity&gt;0&lt;/Capacity&gt;
        ///    &lt;Dimension&gt;0&lt;/Dimension&gt;
        ///    &lt;IndoorCondition&gt;0&lt;/IndoorCondition&gt;
        ///    &lt;Piping&gt;1&lt;/Piping&gt;
        ///    &lt;Weight&gt;0&lt;/Weight&gt;
        ///    &lt;FlowPress&gt;1&lt;/FlowPress&gt;
        ///    &lt;PassWord /&gt;
        ///    &lt;IndoorName&gt;Indr&lt;/IndoorName&gt;
        ///    &lt;OutdoorName&gt;Otdr&lt;/OutdoorName&gt;
        ///  [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_DSM01_G {
            get {
                return ResourceManager.GetString("FGL_DSM01_G", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;FloorItem&gt;
        ///    &lt;Item_Index&gt;1&lt;/Item_Index&gt;
        ///    &lt;Item_Visible&gt;1&lt;/Item_Visible&gt;
        ///    &lt;PreferenceNo&gt;0&lt;/PreferenceNo&gt;
        ///    &lt;Sub_CD&gt;0&lt;/Sub_CD&gt;
        ///    &lt;Msg_No&gt;1002&lt;/Msg_No&gt;
        ///    &lt;Remarks&gt;Floor&lt;/Remarks&gt;
        ///  &lt;/FloorItem&gt;
        ///  &lt;FloorItem&gt;
        ///    &lt;Item_Index&gt;2&lt;/Item_Index&gt;
        ///    &lt;Item_Visible&gt;1&lt;/Item_Visible&gt;
        ///    &lt;PreferenceNo&gt;0&lt;/PreferenceNo&gt;
        ///    &lt;Sub_CD&gt;0&lt;/Sub_CD&gt;
        ///    &lt;Msg_No&gt;1057&lt;/Msg_No&gt;
        ///    &lt;Remarks&gt;Room&lt;/Remarks&gt;
        ///  &lt;/FloorItem&gt;
        ///  &lt;FloorItem&gt;
        ///    &lt;It [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_FloorItemVisible {
            get {
                return ResourceManager.GetString("FGL_FloorItemVisible", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;IndrSrtItem&gt;
        ///    &lt;xIndex&gt;1&lt;/xIndex&gt;
        ///    &lt;srtOrder&gt;1&lt;/srtOrder&gt;
        ///  &lt;/IndrSrtItem&gt;
        ///  &lt;IndrSrtItem&gt;
        ///    &lt;xIndex&gt;2&lt;/xIndex&gt;
        ///    &lt;srtOrder&gt;0&lt;/srtOrder&gt;
        ///  &lt;/IndrSrtItem&gt;
        ///  &lt;IndrSrtItem&gt;
        ///    &lt;xIndex&gt;3&lt;/xIndex&gt;
        ///    &lt;srtOrder&gt;0&lt;/srtOrder&gt;
        ///  &lt;/IndrSrtItem&gt;
        ///  &lt;IndrSrtItem&gt;
        ///    &lt;xIndex&gt;4&lt;/xIndex&gt;
        ///    &lt;srtOrder&gt;0&lt;/srtOrder&gt;
        ///  &lt;/IndrSrtItem&gt;
        ///&lt;/DocumentElement&gt;.
        /// </summary>
        internal static string FGL_IndrSrtItem {
            get {
                return ResourceManager.GetString("FGL_IndrSrtItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;LumpDispItem&gt;
        ///    &lt;xIndex&gt;0&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;1&lt;/DispMode&gt;
        ///  &lt;/LumpDispItem&gt;
        ///  &lt;LumpDispItem&gt;
        ///    &lt;xIndex&gt;1&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;1&lt;/DispMode&gt;
        ///  &lt;/LumpDispItem&gt;
        ///  &lt;LumpDispItem&gt;
        ///    &lt;xIndex&gt;2&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;1&lt;/DispMode&gt;
        ///  &lt;/LumpDispItem&gt;
        ///  &lt;LumpDispItem&gt;
        ///    &lt;xIndex&gt;3&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;1&lt;/DispMode&gt;
        ///  &lt;/LumpDispItem&gt;
        ///  &lt;LumpDispItem&gt;
        ///    &lt;xIndex&gt;4&lt;/xIndex&gt;
        ///    &lt;DispMode&gt;1&lt;/DispMode&gt;
        ///  &lt;/LumpDispItem&gt;
        ///  &lt;LumpD [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_LumpItem {
            get {
                return ResourceManager.GetString("FGL_LumpItem", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;StartUpMode&gt;
        ///    &lt;xIndex&gt;1&lt;/xIndex&gt;
        ///    &lt;StartMode&gt;0&lt;/StartMode&gt;
        ///  &lt;/StartUpMode&gt;
        ///&lt;/DocumentElement&gt;.
        /// </summary>
        internal static string FGL_StartMode {
            get {
                return ResourceManager.GetString("FGL_StartMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to MUoptn_Mid.xml
        ///MUoptn_Sml.xml
        ///MUoptn.xml
        ///&lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;UserOption&gt;
        ///    &lt;Uoption_LNo&gt;2&lt;/Uoption_LNo&gt;
        ///    &lt;Uoption_MNo&gt;1&lt;/Uoption_MNo&gt;
        ///    &lt;Option_MMsg&gt;Services&lt;/Option_MMsg&gt;
        ///  &lt;/UserOption&gt;
        ///&lt;/DocumentElement&gt;
        ///&lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;UserOption&gt;
        ///    &lt;Uoption_LNo&gt;2&lt;/Uoption_LNo&gt;
        ///    &lt;Uoption_MNo&gt;1&lt;/Uoption_MNo&gt;
        ///    &lt;Uoption_SNo&gt;1&lt;/Uoption_SNo&gt;
        ///    &lt;Option_SMsg&gt;Services&lt;/Option_SMsg&gt;
        ///  &lt;/UserOption&gt;
        ///&lt;/DocumentEl [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_UOption {
            get {
                return ResourceManager.GetString("FGL_UOption", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Byte[].
        /// </summary>
        internal static byte[] FGL_UOption1 {
            get {
                object obj = ResourceManager.GetObject("FGL_UOption1", resourceCulture);
                return ((byte[])(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;Userdefault_Power&gt;
        ///    &lt;Material_Kind&gt;1&lt;/Material_Kind&gt;
        ///    &lt;Series&gt;J&lt;/Series&gt;
        ///    &lt;MDB_Kind&gt;0&lt;/MDB_Kind&gt;
        ///    &lt;Type_No&gt;3&lt;/Type_No&gt;
        ///    &lt;Model_No&gt;51&lt;/Model_No&gt;
        ///    &lt;Type_Name&gt;Calibre disjoncteur 16A &lt;/Type_Name&gt;
        ///    &lt;Model_Name&gt;AJYA54LCLR(15A)&lt;/Model_Name&gt;
        ///    &lt;Cable_Size&gt;5.0 to 8.0mm2&lt;/Cable_Size&gt;
        ///    &lt;MCA&gt;20.2&lt;/MCA&gt;
        ///    &lt;MFA&gt;30&lt;/MFA&gt;
        ///    &lt;PS&gt;1N, 230V, 50Hz&lt;/PS&gt;
        ///    &lt;Discon_Flg&gt;1&lt;/Discon_Flg&gt;
        ///    &lt;Unit_Name&gt;Extérieur&lt;/Unit_Name&gt;
        ///    [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_UserdefaultPower {
            get {
                return ResourceManager.GetString("FGL_UserdefaultPower", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;Project_TtlPower&gt;
        ///    &lt;Series&gt;V2&lt;/Series&gt;
        ///    &lt;Outdr_Type&gt;20&lt;/Outdr_Type&gt;
        ///    &lt;tp_Breaker&gt;20&lt;/tp_Breaker&gt;
        ///    &lt;tp_MaxMCA&gt;15&lt;/tp_MaxMCA&gt;
        ///    &lt;tp_Current_Leakage&gt;30&lt;/tp_Current_Leakage&gt;
        ///    &lt;tp_Voltage&gt;230V-50Hz&lt;/tp_Voltage&gt;
        ///    &lt;Cable_Size1 /&gt;
        ///    &lt;Cable_Size2 /&gt;
        ///    &lt;tp_Breaker2&gt;0&lt;/tp_Breaker2&gt;
        ///    &lt;tp_MaxMCA2&gt;0&lt;/tp_MaxMCA2&gt;
        ///    &lt;tp_Voltage2&gt;_&lt;/tp_Voltage2&gt;
        ///    &lt;tp_Current_Leakage2&gt;-1&lt;/tp_Current_Leakage2&gt;
        ///    &lt;Cable_Size1_2&gt;_&lt;/Cable [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string FGL_UserdefaultTotalPower {
            get {
                return ResourceManager.GetString("FGL_UserdefaultTotalPower", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;Language&gt;
        ///    &lt;Msg_No&gt;-1&lt;/Msg_No&gt;
        ///    &lt;Message /&gt;
        ///  &lt;/Language&gt;
        ///  &lt;Language&gt;
        ///    &lt;Msg_No&gt;1&lt;/Msg_No&gt;
        ///    &lt;Message&gt;Le fonctionnement multiple n&apos;est pas possible&lt;/Message&gt;
        ///  &lt;/Language&gt;
        ///  &lt;Language&gt;
        ///    &lt;Msg_No&gt;2&lt;/Msg_No&gt;
        ///    &lt;Message&gt;Erreur! Consultez le gestionnaire des tâches&lt;/Message&gt;
        ///  &lt;/Language&gt;
        ///  &lt;Language&gt;
        ///    &lt;Msg_No&gt;3&lt;/Msg_No&gt;
        ///    &lt;Message&gt;Il n&apos;est pas possible de connecter la base des donnees&lt;/Message&gt;
        ///  &lt;/Language&gt;
        ///  &lt;La [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string French {
            get {
                return ResourceManager.GetString("French", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized resource of type System.Drawing.Bitmap.
        /// </summary>
        internal static System.Drawing.Bitmap Green {
            get {
                object obj = ResourceManager.GetObject("Green", resourceCulture);
                return ((System.Drawing.Bitmap)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {\rtf1\adeflang1025\ansi\ansicpg1252\uc1\adeff43\deff0\stshfdbch0\stshfloch37\stshfhich37\stshfbi37\deflang1033\deflangfe1033\themelang1036\themelangfe0\themelangcs0{\fonttbl{\f0\fbidi \froman\fcharset0\fprq2{\*\panose 02020603050405020304}Times New Roman;}{\f34\fbidi \froman\fcharset0\fprq2{\*\panose 02040503050406030204}Cambria Math;}
        ///{\f37\fbidi \fswiss\fcharset0\fprq2{\*\panose 020f0502020204030204}Calibri;}{\f43\fbidi \fswiss\fcharset128\fprq2{\*\panose 020b0600070205080204}MS UI Gothic;}{\f44\fbidi \ [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string HeaderFujitsu {
            get {
                return ResourceManager.GetString("HeaderFujitsu", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {\stylesheet{\ql \li0\ri0\sa200\sl276\slmult1
        ///\widctlpar\wrapdefault\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \rtlch\fcs1 \af45\afs22\alang1025 \ltrch\fcs0 \fs22\lang1036\langfe1033\loch\f45\hich\af37\dbch\af45\cgrid\langnp1036\langfenp1033 \snext0 \sqformat \spriority0 Normal;}{\*\cs10 
        ///\additive \ssemihidden \sunhideused \spriority1 Default Paragraph Font;}{\*
        ///\ts11\tsrowd\trftsWidthB3\trpaddl108\trpaddr108\trpaddfl3\trpaddft3\trpaddfb3\trpaddfr3\trcbpat1\trcfpat1\tblind0\tblindtype3\tsvertal [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string HeaderGeneral {
            get {
                return ResourceManager.GetString("HeaderGeneral", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;PipeSellAmount&gt;
        ///    &lt;Unit_ID&gt;-1&lt;/Unit_ID&gt;
        ///    &lt;Model_Name /&gt;
        ///    &lt;SellAmount&gt;1&lt;/SellAmount&gt;
        ///    &lt;xSellAmount&gt;1,0&lt;/xSellAmount&gt;
        ///  &lt;/PipeSellAmount&gt;
        ///&lt;/DocumentElement&gt;.
        /// </summary>
        internal static string PipeSellAmount {
            get {
                return ResourceManager.GetString("PipeSellAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;RefrgSellAmount&gt;
        ///    &lt;Unit_ID&gt;-1&lt;/Unit_ID&gt;
        ///    &lt;Model_Name /&gt;
        ///    &lt;SellAmount&gt;1&lt;/SellAmount&gt;
        ///    &lt;xSellAmount&gt;1,00&lt;/xSellAmount&gt;
        ///  &lt;/RefrgSellAmount&gt;
        ///&lt;/DocumentElement&gt;.
        /// </summary>
        internal static string RefrgSellAmount {
            get {
                return ResourceManager.GetString("RefrgSellAmount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;UnitPrice&gt;
        ///    &lt;Currency_No&gt;11&lt;/Currency_No&gt;
        ///    &lt;Material_Kind&gt;1&lt;/Material_Kind&gt;
        ///    &lt;Series&gt;FM&lt;/Series&gt;
        ///    &lt;MDB_Kind&gt;1&lt;/MDB_Kind&gt;
        ///    &lt;Unit_ID&gt;1&lt;/Unit_ID&gt;
        ///    &lt;Model_Name&gt;AOYG45LBT8&lt;/Model_Name&gt;
        ///    &lt;Type_Name&gt;Unité extérieure / 8 postes&lt;/Type_Name&gt;
        ///    &lt;Custun_Code&gt;872020&lt;/Custun_Code&gt;
        ///    &lt;Family_Name&gt;CV1209&lt;/Family_Name&gt;
        ///    &lt;Remarks&gt;Aoyg 45 Lbt8.Ue - Unite Exterieure Climatiseur Multi-Splits 14000 W&lt;/Remarks&gt;
        ///    &lt;Unit_Price0&gt;84 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string UnitPrice_F {
            get {
                return ResourceManager.GetString("UnitPrice_F", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;?xml version=&quot;1.0&quot; standalone=&quot;yes&quot;?&gt;
        ///&lt;DocumentElement&gt;
        ///  &lt;UnitPrice&gt;
        ///    &lt;Currency_No&gt;11&lt;/Currency_No&gt;
        ///    &lt;Material_Kind&gt;1&lt;/Material_Kind&gt;
        ///    &lt;Series&gt;FM&lt;/Series&gt;
        ///    &lt;MDB_Kind&gt;1&lt;/MDB_Kind&gt;
        ///    &lt;Unit_ID&gt;1&lt;/Unit_ID&gt;
        ///    &lt;Model_Name&gt;AOYG45LBT8&lt;/Model_Name&gt;
        ///    &lt;Type_Name&gt;Unité extérieure / 8 postes&lt;/Type_Name&gt;
        ///    &lt;Custun_Code&gt;872020&lt;/Custun_Code&gt;
        ///    &lt;Family_Name&gt;CV1209&lt;/Family_Name&gt;
        ///    &lt;Remarks&gt;Aoyg 45 Lbt8.Ue - Unite Exterieure Climatiseur Multi-Splits 14000 W&lt;/Remarks&gt;
        ///    &lt;Unit_Price0&gt;84 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string UnitPrice_G {
            get {
                return ResourceManager.GetString("UnitPrice_G", resourceCulture);
            }
        }
    }
}
