﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace InstallDS
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string PathDoDSexe = "";

        private void button1_Click(object sender, EventArgs e)
        {

            ////////////////////////////////////////////////////////////////////////////////////////////////////////
            //IMPORTANTE PARA O PROGRAMA ABRIR NA MARCA CERTA: 
            //O zip que você usa nos recursos não pode ser aquele que vem direto do portal fujitsu. 
            //Voce tem de dezipar esse arquivo, lançar DS ao menos uma vez, e então zipar o programa novamente. Só então 
            //que vc deve adicionar esse .zip nos recursos desse projeto. Se vc não fizer isso, DS sempre vai abrir na marca default (fuji),
            //independente de o usuário ter escolhido a marca general. 
            ////////////////////////////////////////////////////////////////////////////////////////////////////////


            try
            {

                MessageBox.Show("Veuillez indiquer où vous voulez installer Design Simulator.", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Information);


                FolderBrowserDialog dialog = new FolderBrowserDialog();

                dialog.RootFolder = Environment.SpecialFolder.Desktop;

                dialog.ShowNewFolderButton = false;

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    string PathInstall = dialog.SelectedPath;

                    string PathConfig = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData).ToString() + @"\Fujitsu general\DesignSimulator";
                    Directory.CreateDirectory(PathConfig);

                    File.WriteAllText(PathConfig + "\\BasicInfo.config", Properties.Resources.BasicInfo);
                    File.WriteAllText(PathConfig + "\\DispInfo.config", Properties.Resources.DispInfo);
                    File.WriteAllText(PathConfig + "\\DXKitMaster.config", Properties.Resources.DXKitMaster);
                    File.WriteAllText(PathConfig + "\\FGL_DispItem.config", Properties.Resources.FGL_DispItem);

                    if(radioAtlantic.Checked)
                        File.WriteAllText(PathConfig + "\\FGL_DSM01.config", Properties.Resources.FGL_DSM01_F);
                    if(radioGeneral.Checked)
                        File.WriteAllText(PathConfig + "\\FGL_DSM01.config", Properties.Resources.FGL_DSM01_G);

                    File.WriteAllText(PathConfig + "\\FGL_FloorItemVisible.config", Properties.Resources.FGL_FloorItemVisible);
                    File.WriteAllText(PathConfig + "\\FGL_IndrSrtItem.config", Properties.Resources.FGL_IndrSrtItem);
                    File.WriteAllText(PathConfig + "\\FGL_LumpItem.config", Properties.Resources.FGL_LumpItem);
                    File.WriteAllText(PathConfig + "\\FGL_StartMode.config", Properties.Resources.FGL_StartMode);
                    File.WriteAllText(PathConfig + "\\FGL_UOption.fgf", Properties.Resources.FGL_UOption);

                    File.WriteAllText(PathConfig + "\\FGL_UserdefaultPower.config", Properties.Resources.FGL_UserdefaultPower);
                    File.WriteAllText(PathConfig + "\\FGL_UserdefaultTotalPower.config", Properties.Resources.FGL_UserdefaultTotalPower);
                    File.WriteAllText(PathConfig + "\\PipeSellAmount.config", Properties.Resources.PipeSellAmount);
                    File.WriteAllText(PathConfig + "\\RefrgSellAmount.config", Properties.Resources.RefrgSellAmount);

                    File.WriteAllText(PathConfig + "\\UnitPrice_F.config", Properties.Resources.UnitPrice_F);
                    File.WriteAllText(PathConfig + "\\UnitPrice_G.config", Properties.Resources.UnitPrice_G);



                    string PathDoZip = PathInstall + "\\DesignSimulator_Fujitsu.zip";

                    Cursor.Current = Cursors.WaitCursor;

                    File.WriteAllBytes(PathDoZip, Properties.Resources.DesignSimulator_Fujitsu);

                    System.IO.Compression.ZipFile.ExtractToDirectory(PathDoZip, PathInstall);

                    string PathDaPasta = PathInstall + "\\DesignSimulator_Fujitsu";
                    File.WriteAllText(PathDaPasta + "\\HeaderFujitsu.rtf", Properties.Resources.HeaderFujitsu);
                    File.WriteAllText(PathDaPasta + "\\HeaderGeneral.rtf", Properties.Resources.HeaderGeneral);
                    File.WriteAllText(PathDaPasta + "\\Language\\French.xml", Properties.Resources.French);
                    Directory.CreateDirectory(PathDaPasta + "\\BKUP MaJ");
                    File.WriteAllText(PathDaPasta + "\\BKUP MaJ\\French.xml", Properties.Resources.French);

                    Cursor.Current = Cursors.Default;

                    File.Delete(PathDoZip);

                    PathDoDSexe = PathDoZip.Remove(PathDoZip.Length - 4) + "\\DesignSimulator.exe";
                                        
                    Process.Start(PathDoDSexe);

                    MessageBox.Show("Installation terminée!\n\nDesign Simulator sera lancé.\n(Ça peut prendre quelques secondes)");

                    Close();

                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
      
    }
}
